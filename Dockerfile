FROM openjdk:8-jdk-alpine

FROM maven:3.5-jdk-8-alpine

WORKDIR /opt
ENV PORT 8080
EXPOSE 8080
COPY * /opt

ENTRYPOINT exec java $JAVA_OPTS -jar -Dspring.profiles.active=production customer-1.0.jar