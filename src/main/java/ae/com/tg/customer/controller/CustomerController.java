package ae.com.tg.customer.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ae.com.tg.customer.controller.data.CustomerModel;
import ae.com.tg.customer.service.CustomerService;

@RestController
@RequestMapping("v1/customer")
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	@GetMapping("{id}")
	public CustomerModel getCustomer(@PathVariable Long id) {
		return customerService.getCustomer(id);
	}	
	
	@PostMapping()
	public CustomerModel addCustomer(@Valid @RequestBody CustomerModel customer) {
		return customerService.insertCustomer(customer);
	}	
	
	@PutMapping("{id}")
	public CustomerModel updateCustomer(@PathVariable Long id, @Valid @RequestBody CustomerModel customer) {
		return customerService.updateCustomer(id, customer);
	}
}
