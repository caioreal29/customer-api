package ae.com.tg.customer.controller;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ae.com.tg.customer.controller.data.TokenModel;
import ae.com.tg.customer.exception.InvalidLoginException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@RequestMapping("v1/auth")
public class AuthController {

	private static final long EXPIRATION_TIME = 86400000;
	private static final String TOKEN_TYPE = "Bearer";

	@PostMapping
	public TokenModel login(@RequestParam("user") String username, @RequestParam("password") String pwd) {
	
		//Performing a very simple validation, just to give some authentication to access
		if (!username.equals("Id1") || !pwd.equals("123")) {
			throw new InvalidLoginException();
		}
		
		TokenModel tokenModel = new TokenModel();
		String token = getJWTToken(username);
		tokenModel.setToken(token);
		tokenModel.setType(TOKEN_TYPE);
		tokenModel.setExpiresIn(EXPIRATION_TIME);
		
		return tokenModel;
	}

	private String getJWTToken(String username) {
		String secretKey = "mySecretKey";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ROLE_USER");
		
		String token = Jwts
				.builder()
				.setId("softtekJWT")
				.setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512,
						secretKey.getBytes()).compact();

		return token;
	}
}