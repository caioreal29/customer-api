package ae.com.tg.customer.controller.data;

import lombok.Data;

@Data
public class TokenModel {
	private String token;
	private String type;
	private long expiresIn;
}
