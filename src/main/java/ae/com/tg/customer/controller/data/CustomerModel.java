package ae.com.tg.customer.controller.data;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class CustomerModel {
	private Long id;
	
	@NotNull(message = "clientId must not be null")
	@NotBlank(message = "clientId must not be empty")
	private String clientId;
	
	private String name;
	private String phoneNumber;
	private String address;
	private String communicationMethod;
}
