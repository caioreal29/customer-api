package ae.com.tg.customer.controller.data;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

import lombok.Data;

@Data
public class ApiError {

	   private HttpStatus status;
	   private String timestamp;
	   private String message;

	   private ApiError() {
	       timestamp = LocalDateTime.now().toString();
	   }

	   public ApiError(HttpStatus status) {
	       this();
	       this.status = status;
	   }

	   public ApiError(HttpStatus status, Throwable ex) {
	       this();
	       this.status = status;
	       this.message = "Unexpected error";
	   }

	   public ApiError(HttpStatus status, String message, Throwable ex) {
	       this(status, ex);
	       this.message = message;
	   }
	}