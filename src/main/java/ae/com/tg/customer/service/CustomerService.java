package ae.com.tg.customer.service;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ae.com.tg.customer.controller.data.CustomerModel;
import ae.com.tg.customer.exception.ClientNotFoundException;
import ae.com.tg.customer.integration.data.Customer;
import ae.com.tg.customer.integration.repository.CustomerRepository;

@Service
public class CustomerService {

	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	private CustomerRepository repository;
	
	public CustomerModel insertCustomer(CustomerModel customer) {
		Customer customerEntity = mapper.map(customer, Customer.class);
		 
		Customer savedCustomer = repository.save(customerEntity);
		 
		return mapper.map(savedCustomer, CustomerModel.class);
	}	
	
	public CustomerModel getCustomer(Long id) {
		Optional<Customer> customerEntity = getCustomerInDb(id);
		
		CustomerModel customer = mapper.map(customerEntity.get(), CustomerModel.class);
		
		return customer;
	}
	
	public CustomerModel updateCustomer(Long id, CustomerModel customer) {
		//Just to check if the customer exists
		Optional<Customer> customerInDb = getCustomerInDb(id);
		
		Customer customerEntity = mapper.map(customer, Customer.class);
		 
		customerEntity.setId(customerInDb.get().getId());
		
		Customer savedCustomer = repository.save(customerEntity);
		 
		return mapper.map(savedCustomer, CustomerModel.class);
	}

	private Optional<Customer> getCustomerInDb(Long id) {
		Optional<Customer> customerEntity = repository.findById(id);
		if(!customerEntity.isPresent()) {
			throw new ClientNotFoundException(id);
		}
		return customerEntity;
	}	
}
