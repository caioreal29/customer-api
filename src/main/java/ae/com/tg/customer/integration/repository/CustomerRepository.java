package ae.com.tg.customer.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ae.com.tg.customer.integration.data.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
