package ae.com.tg.customer.integration.data;

public enum CommunicationMethod {
	EMAIL, SMS, POST, ALL
}
