package ae.com.tg.customer.config;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfig {

	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();

		// Official configuration instructions:
		// http://modelmapper.org/user-manual/configuration/
		// Exact match
		modelMapper.getConfiguration().setFullTypeMatchingRequired(true);

		// The matching strategy uses strict mode
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		return modelMapper;
	}

}