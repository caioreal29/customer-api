package ae.com.tg.customer.handlers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import ae.com.tg.customer.controller.data.ApiError;
import ae.com.tg.customer.exception.ClientNotFoundException;
import ae.com.tg.customer.exception.InvalidLoginException;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

   @Override
   protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
       String error = "Malformed JSON request";
       return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, error, ex));
   }

   private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
       return new ResponseEntity<>(apiError, apiError.getStatus());
   }
   
   @ExceptionHandler(ClientNotFoundException.class)
   protected ResponseEntity<Object> handleEntityNotFound(
           ClientNotFoundException ex) {
       ApiError apiError = new ApiError(HttpStatus.NOT_FOUND);
       apiError.setMessage("Client with id: " + ex.getId() + " not found");
       return buildResponseEntity(apiError);
   }
   
   @ExceptionHandler(InvalidLoginException.class)
   protected ResponseEntity<Object> handleEntityNotFound(
		   InvalidLoginException ex) {
       ApiError apiError = new ApiError(HttpStatus.UNAUTHORIZED);
       apiError.setMessage("Invalid Login");
       return buildResponseEntity(apiError);
   }
   
   // error handle for @Valid
   @Override
   protected ResponseEntity<Object>
   handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                HttpHeaders headers,
                                HttpStatus status, WebRequest request) {

	   ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST);
       apiError.setMessage(ex.getFieldError().getDefaultMessage());
       return buildResponseEntity(apiError);

   }
}