package ae.com.tg.customer.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ClientNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -883684630112691428L;
	
	private Long id;
	
	public ClientNotFoundException(Long id) {
		this.id = id;
	}
}
