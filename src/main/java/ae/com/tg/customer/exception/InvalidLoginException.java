package ae.com.tg.customer.exception;

/**
 * 
 * @author caio.martins
 * @version 1.0.0
 * 
 * This is used when the login in this API has failed.
 *
 */
public class InvalidLoginException extends RuntimeException {

	private static final long serialVersionUID = -883684630112691428L;
	
}
