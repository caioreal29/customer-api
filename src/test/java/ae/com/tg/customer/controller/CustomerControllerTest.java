package ae.com.tg.customer.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import ae.com.tg.customer.CustomerApplication;
import ae.com.tg.customer.controller.data.ApiError;
import ae.com.tg.customer.controller.data.CustomerModel;
import ae.com.tg.customer.exception.ClientNotFoundException;
import ae.com.tg.customer.service.CustomerService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CustomerApplication.class)
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles(value = "test")
public class CustomerControllerTest {
	
	private static final long VALID_ID = 1L;

	@Autowired
    private MockMvc mvc;
	
	@MockBean
	private CustomerService customerService;
	
	@Test
	public void getCustomerOk()
	  throws Exception {

		Mockito.when(customerService.getCustomer(VALID_ID)).thenReturn(getCustomerAdded());

		 ResultActions getResult = mvc.perform(MockMvcRequestBuilders.get("/v1/customer/"+VALID_ID)
	      .contentType(MediaType.APPLICATION_JSON))
	      .andExpect(MockMvcResultMatchers.status().isOk())
	      .andExpect(MockMvcResultMatchers.content()
	      .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
		 
		 MockHttpServletResponse response = getResult.andReturn().getResponse();
		    
		 CustomerModel customerModel = new ObjectMapper().readValue(response.getContentAsString(), CustomerModel.class);
		 
		 assertNotNull(customerModel.getId());
	}
	
	@Test
	public void getCustomerNotFound()
	  throws Exception {

		Mockito.when(customerService.getCustomer(Mockito.anyLong())).thenThrow(new ClientNotFoundException(787866L));

		 ResultActions getResult = mvc.perform(MockMvcRequestBuilders.get("/v1/customer/787866")
	      .contentType(MediaType.APPLICATION_JSON))
	      .andExpect(MockMvcResultMatchers.status().isNotFound())
	      .andExpect(MockMvcResultMatchers.content()
	      .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
		 
		 MockHttpServletResponse response = getResult.andReturn().getResponse();
		    
		 ApiError apiErrorReturned = new ObjectMapper().readValue(response.getContentAsString(), ApiError.class);
		 
		 ApiError apiErrorExpected = getApiErrorNotFound(787866L);
		 
		 assertEquals(apiErrorExpected.getStatus(), apiErrorReturned.getStatus());
		 assertEquals(apiErrorExpected.getMessage(), apiErrorReturned.getMessage());
	}
	
	@Test
	public void addCustomerBadRequest() throws Exception {
		CustomerModel newCustomer = getCustomerForAddBadRequest();
		String reqBody = new ObjectMapper().writeValueAsString(newCustomer);

		 ResultActions getResult = mvc.perform(MockMvcRequestBuilders.post("/v1/customer").content(reqBody)
	      .contentType(MediaType.APPLICATION_JSON))
	      .andExpect(MockMvcResultMatchers.status().isBadRequest())
	      .andExpect(MockMvcResultMatchers.content()
	      .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
		 
		 MockHttpServletResponse response = getResult.andReturn().getResponse();
		    
		 ApiError apiErrorReturned = new ObjectMapper().readValue(response.getContentAsString(), ApiError.class);
		 
		 ApiError apiErrorExpected = getApiErrorBadRequest();
		 
		 assertEquals(apiErrorExpected.getStatus(), apiErrorReturned.getStatus());
	}
	
	@Test
	public void addCustomerOk()
	  throws Exception {
		
		CustomerModel newCustomer = getCustomerForAddOk();
		String reqBody = new ObjectMapper().writeValueAsString(newCustomer);

		Mockito.when(customerService.insertCustomer(newCustomer)).thenReturn(getCustomerAdded());
		
	    ResultActions andExpect = mvc.perform(MockMvcRequestBuilders.post("/v1/customer").content(reqBody)
	      .contentType(MediaType.APPLICATION_JSON))
	      .andExpect(MockMvcResultMatchers.status().isOk());
	    
	    MockHttpServletResponse response = andExpect.andReturn().getResponse();
	    
	    CustomerModel customerModel = new ObjectMapper().readValue(response.getContentAsString(), CustomerModel.class);
	    
	    assertNotNull(customerModel.getId());
	}
	
	@Test
	public void updateCustomerOk()
	  throws Exception {
		
		CustomerModel newCustomer = getCustomerForUpdateOk();
		String reqBody = new ObjectMapper().writeValueAsString(newCustomer);

		Mockito.when(customerService.updateCustomer(VALID_ID, newCustomer)).thenReturn(getCustomerForUpdateOk());
		
	    ResultActions andExpect = mvc.perform(MockMvcRequestBuilders.put("/v1/customer/"+VALID_ID).content(reqBody)
	      .contentType(MediaType.APPLICATION_JSON))
	      .andExpect(MockMvcResultMatchers.status().isOk());
	    
	    MockHttpServletResponse response = andExpect.andReturn().getResponse();
	    
	    CustomerModel customerModel = new ObjectMapper().readValue(response.getContentAsString(), CustomerModel.class);
	    
	    assertNotNull(customerModel.getId());
	    assertEquals(newCustomer, customerModel);
	}
	
	@Test
	public void updateCustomerNotFoundOk()
	  throws Exception {
		CustomerModel newCustomer = getCustomerForUpdateOk();
		String reqBody = new ObjectMapper().writeValueAsString(newCustomer);
		
		Mockito.when(customerService.updateCustomer(Mockito.anyLong(), Mockito.any(CustomerModel.class))).thenThrow(new ClientNotFoundException(787866L));
		
	    ResultActions andExpect = mvc.perform(MockMvcRequestBuilders.put("/v1/customer/787866").content(reqBody)
	      .contentType(MediaType.APPLICATION_JSON))
	      .andExpect(MockMvcResultMatchers.status().isNotFound());
	    
	    MockHttpServletResponse response = andExpect.andReturn().getResponse();
	    	    
	    ApiError apiErrorReturned = new ObjectMapper().readValue(response.getContentAsString(), ApiError.class);
		 
		ApiError apiErrorExpected = getApiErrorNotFound(787866L);
		 
		assertEquals(apiErrorExpected.getStatus(), apiErrorReturned.getStatus());
		assertEquals(apiErrorExpected.getMessage(), apiErrorReturned.getMessage());
	}	
	
	private static CustomerModel getCustomerForAddOk() {
		CustomerModel customerModel = new CustomerModel();
		customerModel.setClientId("clientId1");
		customerModel.setAddress("Street 9, Dubai, UAE");
		customerModel.setCommunicationMethod("SMS");
		customerModel.setName("John");
		customerModel.setPhoneNumber("8787878787");
		
		return customerModel;
	}
	
	private static CustomerModel getCustomerForUpdateOk() {
		CustomerModel customerModel = new CustomerModel();
		customerModel.setId(VALID_ID);
		customerModel.setClientId("clientNewClientId");
		customerModel.setAddress("Street 9, Dubai, UAE");
		customerModel.setCommunicationMethod("EMAIL");
		customerModel.setName("John");
		customerModel.setPhoneNumber("3287838278");
		
		return customerModel;
	}
	
	private static CustomerModel getCustomerForAddBadRequest() {
		//At this moment a bad request is just when it doesn't come with a client id
		CustomerModel customerModel = new CustomerModel();
		customerModel.setAddress("Street 9, Dubai, UAE");
		customerModel.setCommunicationMethod("SMS");
		customerModel.setName("John");
		customerModel.setPhoneNumber("8787878787");
		
		return customerModel;
	}
	
	private static CustomerModel getCustomerAdded() {
		CustomerModel customerModel = new CustomerModel();
		customerModel.setId(VALID_ID);
		customerModel.setClientId("clientId1");
		customerModel.setAddress("Street 9, Dubai, UAE");
		customerModel.setCommunicationMethod("SMS");
		customerModel.setName("John");
		customerModel.setPhoneNumber("8787878787");
		
		return customerModel;
	}
	
	private static ApiError getApiErrorNotFound(Long id) {
		ApiError apiError = new ApiError(HttpStatus.NOT_FOUND);
		apiError.setMessage("Client with id: " + id + " not found");
		
		return apiError;
	}
	
	private static ApiError getApiErrorBadRequest() {
		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST);
		apiError.setMessage("clientId must not be empty");
		
		return apiError;
	}	
}
