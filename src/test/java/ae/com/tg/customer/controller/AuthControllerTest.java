package ae.com.tg.customer.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import ae.com.tg.customer.CustomerApplication;
import ae.com.tg.customer.controller.data.ApiError;
import ae.com.tg.customer.controller.data.CustomerModel;
import ae.com.tg.customer.controller.data.TokenModel;
import ae.com.tg.customer.exception.ClientNotFoundException;
import ae.com.tg.customer.service.CustomerService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CustomerApplication.class)
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles(value = "test")
public class AuthControllerTest {

	@Autowired
    private MockMvc mvc;
	
	@Test
	public void loginOk() throws Exception {
		 ResultActions getResult = mvc.perform(MockMvcRequestBuilders.post("/v1/auth/").param("user", "Id1").param("password", "123")
	      .contentType(MediaType.APPLICATION_JSON))
	      .andExpect(MockMvcResultMatchers.status().isOk())
	      .andExpect(MockMvcResultMatchers.content()
	      .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
		 
		 MockHttpServletResponse response = getResult.andReturn().getResponse();
		    
		 TokenModel customerModel = new ObjectMapper().readValue(response.getContentAsString(), TokenModel.class);
		 
		 assertNotNull(customerModel.getToken());
		 assertNotNull(customerModel.getExpiresIn());
		 assertEquals("Bearer", customerModel.getType());
	}
	
	@Test
	public void loginNotOk() throws Exception {
		 ResultActions getResult = mvc.perform(MockMvcRequestBuilders.post("/v1/auth/").param("user", "Id1").param("password", "2323")
	      .contentType(MediaType.APPLICATION_JSON))
	      .andExpect(MockMvcResultMatchers.status().isUnauthorized())
	      .andExpect(MockMvcResultMatchers.content()
	      .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
		 
		 MockHttpServletResponse response = getResult.andReturn().getResponse();
		    
		 ApiError apiError = new ObjectMapper().readValue(response.getContentAsString(), ApiError.class);
		 assertEquals(HttpStatus.UNAUTHORIZED, apiError.getStatus());
	}
}
