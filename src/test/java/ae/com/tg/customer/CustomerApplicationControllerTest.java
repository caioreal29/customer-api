package ae.com.tg.customer;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ae.com.tg.customer.controller.AuthController;
import ae.com.tg.customer.controller.CustomerController;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = CustomerApplication.class)
@ActiveProfiles(value = "test")
public class CustomerApplicationControllerTest {
	
	@Autowired
	private CustomerController cc;
	
	@Autowired
	private AuthController ac;

	@Test
	public void contextLoads() {
		assertNotNull(cc);
		assertNotNull(ac);
	}
}
