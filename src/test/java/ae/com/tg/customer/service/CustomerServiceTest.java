package ae.com.tg.customer.service;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import ae.com.tg.customer.CustomerApplication;
import ae.com.tg.customer.controller.data.CustomerModel;
import ae.com.tg.customer.exception.ClientNotFoundException;
import ae.com.tg.customer.integration.data.CommunicationMethod;
import ae.com.tg.customer.integration.data.Customer;
import ae.com.tg.customer.integration.repository.CustomerRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CustomerApplication.class)
@ActiveProfiles(value = "test")
public class CustomerServiceTest {

	private static final long INVALID_CLIENT_ID = 23223L;

	private static final long VALID_CLIENT_ID = 1L;

	@Autowired
	private CustomerService customerService;

	@MockBean
	private CustomerRepository customerRepository;

	@Test
	public void getCustomerOk() throws Exception {

		Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(getCustomer(VALID_CLIENT_ID)));

		CustomerModel customer = this.customerService.getCustomer(VALID_CLIENT_ID);

		assertEquals(VALID_CLIENT_ID, customer.getId().longValue());
	}

	@Test(expected = ClientNotFoundException.class)
	public void getCustomerClientNotFound() throws Exception {

		Mockito.when(customerRepository.findById(Mockito.anyLong())).thenThrow(ClientNotFoundException.class);
		CustomerModel customer = this.customerService.getCustomer(INVALID_CLIENT_ID);
	}
	
	@Test
	public void insertCustomer() throws Exception {	
		Mockito.when(customerRepository.save(Mockito.any())).thenReturn(getCustomer(VALID_CLIENT_ID));

		CustomerModel customer = this.customerService.insertCustomer(getCustomerForAddOk());

		assertEquals(1L, customer.getId().longValue());
	}
	
	@Test
	public void updateCustomer() throws Exception {	
		Customer customer = getCustomer(1L);
		
		Mockito.when(customerRepository.findById(VALID_CLIENT_ID)).thenReturn(Optional.of(getCustomer(VALID_CLIENT_ID)));

		Mockito.when(customerRepository.save(Mockito.any())).thenReturn(customer);

		CustomerModel customerUpdated = this.customerService.updateCustomer(VALID_CLIENT_ID, getCustomerForAddOk());

		assertEquals(VALID_CLIENT_ID, customer.getId().longValue());
		assertNotEquals(customerUpdated, customer);
	}
	
	@Test(expected = ClientNotFoundException.class)
	public void updateCustomerClientNotFound() throws Exception {	
		Mockito.when(customerRepository.save(Mockito.any())).thenThrow(ClientNotFoundException.class);

		this.customerService.updateCustomer(1L, getCustomerForAddOk());
	}

	private static Customer getCustomer(Long id) {
		Customer customer = new Customer();
		customer.setId(id);
		customer.setClientId("clientId1");
		customer.setAddress("Street 9, Dubai, UAE");
		customer.setCommunicationMethod(CommunicationMethod.SMS);
		customer.setName("John");
		customer.setPhoneNumber("8787878787");

		return customer;
	}
	
	private static CustomerModel getCustomerForAddOk() {
		CustomerModel customerModel = new CustomerModel();
		customerModel.setClientId("clientId2");
		customerModel.setAddress("Street 9, Dubai, UAE");
		customerModel.setCommunicationMethod("SMS");
		customerModel.setName("John");
		customerModel.setPhoneNumber("8787878787");
		
		return customerModel;
	}
}
